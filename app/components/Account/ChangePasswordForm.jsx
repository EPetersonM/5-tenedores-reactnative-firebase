import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import { size } from 'lodash';
import firebase from 'firebase/app';
import { reauthenticate } from '../../utils/api';

const ChangePasswordForm = (props) => {
	const { setShowModal, toastRef } = props;
	const [showPassword, setShowPassword] = useState(false);
	const [formData, setFormData] = useState(defaultValue());
	const [errors, setErrors] = useState({});
	const [isLoading, setIsLoading] = useState(false);

	const onChange = (e, type) => {
		setFormData({ ...formData, [type]: e.nativeEvent.text });
	};

	const onSubmit = async () => {
		let isSetError = true;
		let errorsTemp = {};
		setErrors({});

		if (
			!formData.password ||
			!formData.newPassword ||
			!formData.repeatNewPassword
		) {
			errorsTemp = {
				password: !formData.password
					? 'La contraseña no puede estar vacia'
					: '',
				newPassword: !formData.newPassword
					? 'La contraseña no puede estar vacia'
					: '',
				repeatNewPassword: !formData.repeatNewPassword
					? 'La contraseña no puede estar vacia'
					: '',
			};
		} else if (formData.newPassword !== formData.repeatNewPassword) {
			errorsTemp = {
				newPassword: 'Las contraseñas no son iguales',
				repeatNewPassword: 'Las contraseñas no son iguales',
			};
		} else if (size(formData.newPassword) < 6) {
			errorsTemp = {
				newPassword: 'La contraseña tiene que ser mayor a 5 caracteres',
				repeatNewPassword: 'La contraseña tiene que ser mayor a 5 caracteres',
			};
		} else {
			setIsLoading(true);
			await reauthenticate(formData.password)
				.then(async () => {
					await firebase
						.auth()
						.currentUser.updatePassword(formData.newPassword)
						.then(() => {
							isSetError = false;
							setIsLoading(false);
							setShowModal(false);
							firebase.auth().signOut();
						})
						.catch(() => {
							errorsTemp = {
								other: 'Error al actualizar la contraseña',
							};
							setIsLoading(false);
						});
				})
				.catch(() => {
					errorsTemp = {
						password: 'La contraseña es incorrecta',
					};
					setIsLoading(false);
				});
		}

		isSetError && setErrors(errorsTemp);
	};

	return (
		<View style={styles.view}>
			<Input
				placeholder='Ingrese la contraseña actual'
				containerStyle={styles.input}
				secureTextEntry={showPassword ? false : true}
				rightIcon={
					<Icon
						type='material-community'
						name={showPassword ? 'eye-off-outline' : 'eye-outline'}
						color='#c2c2c2'
						onPress={() => setShowPassword(!showPassword)}
					/>
				}
				onChange={(e) => onChange(e, 'password')}
				errorMessage={errors.password}
			/>
			<Input
				placeholder='Ingrese la nueva contraseña'
				containerStyle={styles.input}
				secureTextEntry={showPassword ? false : true}
				rightIcon={
					<Icon
						type='material-community'
						name={showPassword ? 'eye-off-outline' : 'eye-outline'}
						color='#c2c2c2'
						onPress={() => setShowPassword(!showPassword)}
					/>
				}
				onChange={(e) => onChange(e, 'newPassword')}
				errorMessage={errors.newPassword}
			/>
			<Input
				placeholder='Repita la nueva contraseña'
				containerStyle={styles.input}
				secureTextEntry={showPassword ? false : true}
				rightIcon={
					<Icon
						type='material-community'
						name={showPassword ? 'eye-off-outline' : 'eye-outline'}
						color='#c2c2c2'
						onPress={() => setShowPassword(!showPassword)}
					/>
				}
				onChange={(e) => onChange(e, 'repeatNewPassword')}
				errorMessage={errors.repeatNewPassword}
			/>
			<Text>{errors.other}</Text>
			<Button
				title='Actualizar Contraseña'
				containerStyle={styles.btnContainer}
				buttonStyle={styles.btn}
				onPress={onSubmit}
				loading={isLoading}
			/>
		</View>
	);
};

const defaultValue = () => {
	return {
		password: '',
		newPassword: '',
		repeatNewPassword: '',
	};
};
const styles = StyleSheet.create({
	view: {
		alignItems: 'center',
		paddingTop: 10,
		paddingBottom: 10,
	},
	input: {
		marginBottom: 10,
	},
	btnContainer: {
		width: '95%',
		marginTop: 20,
	},
	btn: {
		backgroundColor: '#00a680',
	},
});

export default ChangePasswordForm;
