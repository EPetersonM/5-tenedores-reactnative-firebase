import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Avatar } from 'react-native-elements';
import firebase from 'firebase/app';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

const InfoUser = (props) => {
	const { userInfo, toastRef, setLoading, setLoadingText } = props;
	const { photoURL, displayName, email, uid } = userInfo;

	const changeAvatar = async () => {
		const resultPermission = await Permissions.askAsync(
			Permissions.MEDIA_LIBRARY
		);
		const resultPermissionCamera =
			resultPermission.permissions.mediaLibrary.status;

		if (resultPermissionCamera === 'denied') {
			toastRef.current.show('Es necesario otorgar los permisos de la galeria.');
		} else {
			const result = await ImagePicker.launchImageLibraryAsync({
				allowsEditing: true,
				aspect: [4, 3],
			});

			if (result.cancelled) {
				toastRef.current.show('Ha cancelado la selección de imágenes.');
			} else {
				uploadImage(result.uri)
					.then(() => {
						updatePhotoUrl();
					})
					.catch(() => {
						toastRef.current.show('Error al actualizar el avatar');
					});
			}
		}
	};

	const uploadImage = async (uri) => {
		setLoadingText('Actualizando el avatar');
		setLoading(true);

		const response = await fetch(uri);
		const blob = await response.blob();

		const ref = firebase.storage().ref().child(`avatar/${uid}`);
		return ref.put(blob);
	};

	const updatePhotoUrl = () => {
		firebase
			.storage()
			.ref(`avatar/${uid}`)
			.getDownloadURL()
			.then(async (response) => {
				const update = {
					photoURL: response,
				};
				await firebase.auth().currentUser.updateProfile(update);
				setLoading(false);
			})
			.catch(() => {
				toastRef.current.show('Error al actualizar el avatar.');
			});
	};

	return (
		<View style={styles.viewUserInfo}>
			<Avatar
				rounded
				size='large'
				containerStyle={styles.userInfoAvatar}
				icon={{ name: 'home' }}
				source={
					photoURL
						? { uri: photoURL }
						: require('../../../assets/img/avatar-default.jpg')
				}>
				<Avatar.Accessory
					size={25}
					onPress={changeAvatar}
					style={{ backgroundColor: '#00a680' }}
				/>
			</Avatar>
			<View>
				<Text style={styles.displayName}>
					{displayName ? displayName : 'Anónimo'}
				</Text>
				<Text>{email ? email : 'Social Login'}</Text>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	viewUserInfo: {
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
		backgroundColor: '#f2f2f2',
		paddingTop: 30,
		paddingBottom: 30,
	},
	userInfoAvatar: {
		marginRight: 20,
	},
	displayName: {
		fontWeight: 'bold',
		paddingBottom: 10,
	},
});

export default InfoUser;
