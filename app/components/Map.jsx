import React from 'react';
import MapView from 'react-native-maps';
import openMap from 'react-native-open-maps';

const Map = (props) => {
	const { location, name, height } = props;

	const openAppMap = () => {
		openMap({
			// latitude: location.latitude,
			// longitude: location.longitude,
			zoom: 19,
			query: `${location.latitude},${location.longitude}`,
		});
	};

	return (
		<MapView
			onPress={openAppMap}
			style={{ height: height, width: '100%' }}
			initialRegion={location}>
			<MapView.Marker
				coordinate={{
					latitude: location.latitude,
					longitude: location.longitude,
				}}
			/>
		</MapView>
	);
};

export default Map;
